package com.swagger.model.PetData;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor

@Data
@Builder
public class PetCategory {
    private int id;
    private String name;
}
