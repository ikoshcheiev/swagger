package com.swagger.model.PetData;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor

@Data
@Builder
public class PetInvalidIdSupplied {
    private String id;
    private String name;
    private PetCategory category;
    private List<String> photoUrls;
    private List<PetTags> tags;

    private String status;
}
