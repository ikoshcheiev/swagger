package com.swagger.cucumber.serenity;

import com.swagger.model.PetData.Pet;
import com.swagger.model.PetData.PetCategory;
import com.swagger.model.PetData.PetInvalidIdSupplied;
import com.swagger.model.PetData.PetTags;
import com.swagger.utils.ReuseableSpecifications;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.hamcrest.MatcherAssert;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;

/**
 * Class which describes Pet endpoint interactions
 *
 * @author IKoshcheiev "koshcheiev.vanya@gmail.com"
 * @version 1.0
 * <p>
 * Available types: POST | PUT | GET | DELETE
 * @see Pet
 */
public class PetSerenitySteps {

    /**
     * GET
     * curl -X GET "https://petstore.swagger.io/v2/pet/12" -H  "accept: application/json"
     * This method Finds Pet by pet id.
     *
     * @param petId ID of pet object (Required)
     * @return 200 - Undocumented. Pet objects as ValidatableResponse object or 404 - Undocumented or 405 Invalid input
     */
    @Step("Get pet by petId: {0}")
    public ValidatableResponse getPetById(int petId) {
        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .get("/pet/" + petId)
                .then()
                .log().all();
    }

    /**
     * GET
     * curl -X GET "https://petstore.swagger.io/v2/pet/findByStatus?status=available" -H  "accept: application/json"
     * This method Finds Pets by status. Multiple status values can be provided with comma separated strings
     *
     * @param status Status of pet object. (Required) Available values : available, pending, sold
     * @return 200 - Successfull operation. List of Pet objects as ValidatableResponse object. or 400 - Invalid status value
     */
    @Step("Send GET request to receive all pets by status")
    public ValidatableResponse getPetsByStatus(String status) {
        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when().log().all()
                .param("status", status)
                .get("/pet/findByStatus")
                .then().log().all();
    }

    /**
     * @param tagNames List of pet tags. (Required)
     * @return 200 - Successfull operation. List of Pet objects as ValidatableResponse object. or 400 - Invalid tag value
     * @see PetTags
     * @deprecated v.2020
     * GET
     * curl -X GET "https://petstore.swagger.io/v2/pet/findByTags?tags=string1" -H  "accept: application/json"
     * Multiple tags can be provided with comma separated strings. Use tag1, tag2, tag3 for testing.
     */
    @Step("Get pet data by Tags via GET request")
    public ValidatableResponse getPetByTags(List<String> tagNames) {
        String tags = String.join("&", tagNames);

        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .get("/pet/findByTags?tags=" + tags)
                .then()
                .log().all();
    }

    /**
     * DELETE
     * curl -X DELETE "https://petstore.swagger.io/v2/pet/1" -H  "accept: application/json" -H  "api_key: special-key"
     * This method removes pet by ID.
     *
     * @param petId ID of pet object (Required)
     * @return 200 - Undocumented. ValidatableResponse object where response body consists of code/type/message. or 400 - Invalid ID supplied or 404 - Pet not found
     */
    @Step("Remove pet by petId: {0} via DELETE request")
    public ValidatableResponse removePetById(int petId) {
        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .contentType(ContentType.JSON)
                .header("api_key", "special-key")
                .delete("/pet/" + petId)
                .then()
                .log().all();
    }

    /**
     * POST
     * curl -X POST "https://petstore.swagger.io/v2/pet/1/uploadImage" -H  "accept: application/json" -H  "Content-Type: multipart/form-data" -F "additionalMetadata=test" -F "file=fileName.jpg;type=image/jpeg"
     * This method uploads image for exact pet
     *
     * @param petId              ID of pet to update (Required)
     * @param fileName           Image file name with extension to upload (Optional)
     * @param additionalMetadata Additional data to pass to server (Optional)
     * @return 200 - Successfull operation. ValidatableResponse object where response body consists of code/type/message.
     */
    @Step("Upload an image to pet with  petId: {0} via POST request")
    public ValidatableResponse uploadImage(int petId, String fileName, String additionalMetadata) {
        File file = new File("src\\test\\resources\\" + fileName);

        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .contentType("multipart/form-data")
                .header("additionalMetadata", additionalMetadata)
                .log().all()
                .when()
                .multiPart("file", file, "multipart/form-data")
                .post(String.format("/pet/%s/uploadImage", petId))
                .then()
                .log().all();
    }

    /**
     * This method helps to create a pet object
     *
     * @param petId        ID of pet object. (Required)
     * @param petName      Name of pet object (Optional)
     * @param categoryId   Category of category in pet object (Optional)
     * @param categoryName Category Name of category in pet object (Optional)
     * @param photoUrls    List of photo urls of pet object (Optional)
     * @param tagIds       List of tag ids urls of tag in pet object (Optional)
     * @param tagNames     List of tag names urls of tag in pet object (Optional)
     * @param petStatus    Status of pet object. Any values : available, pending, sold, etc. (Optional)
     * @return This method return new pet object.
     * @see Pet
     * @see PetCategory
     * @see PetTags
     */
    @Step("Prepare Pet object: {0}")
    public Pet createPetObject(int petId, String petName, int categoryId, String categoryName,
                               String photoUrls, String tagIds, String tagNames, String petStatus) {

        List<String> urls = new ArrayList<>(Arrays.asList(photoUrls.split(";").clone()));

        PetCategory petCategory = new PetCategory(categoryId, categoryName);

        List<PetTags> petTagList = new ArrayList<>();
        String[] ids = tagIds.split(";");
        String[] names = tagNames.split(";");
        for (int i = 0; i < ids.length; i++) {
            petTagList.add(new PetTags(Integer.parseInt(ids[i]), names[i]));
        }
        return Pet.builder().id(petId).category(petCategory).name(petName).photoUrls(urls).tags(petTagList).status(petStatus).build();
    }

    /**
     * POST
     * curl -X POST "https://petstore.swagger.io/v2/pet" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"id\": 123,  \"category\": {    \"id\": 0,    \"name\": \"string\"  },  \"name\": \"doggie\",  \"photoUrls\": [    \"string\"  ],  \"tags\": [    {      \"id\": 0,      \"name\": \"string\"    }  ],  \"status\": \"available\"}"
     * This method adds a new pet to the store
     *
     * @return 200 - Undocumented. ValidatableResponse object where response body contains new pet object. or 405 - Invalid input
     */
    @Step("Create Pet with all data, perId: {0} via POST request")
    public ValidatableResponse createPet(int petId, String petName, int categoryId, String categoryName,
                                         String photoUrls, String tagIds, String tagNames, String petStatus) {
        Pet pet = createPetObject(petId, petName, categoryId, categoryName, photoUrls, tagIds, tagNames, petStatus);

        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .log().all()
                .body(pet)
                .post("/pet")
                .then()
                .log().all();
    }

    /**
     * POST
     * curl -X POST "https://petstore.swagger.io/v2/pet" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"id\": 123,  \"category\": {    \"id\": 0,    \"name\": \"string\"  },  \"name\": \"doggie\",  \"photoUrls\": [    \"string\"  ],  \"tags\": [    {      \"id\": 0,      \"name\": \"string\"    }  ],  \"status\": \"available\"}"
     * This method creates pet only by pet Id.
     *
     * @param petId ID of pet object. (Required)
     * @return 200 - Undocumented. ValidatableResponse object where response body contains new pet object or 405 - Invalid input
     */
    @Step("Create Pet only by providing petId: {0} via POST request")
    public ValidatableResponse createPet(int petId) {
        Pet pet = Pet.builder().id(petId).build();
        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .body(pet)
                .post("/pet")
                .then()
                .log().all();
    }

    /**
     * POST
     * curl -X POST "https://petstore.swagger.io/v2/pet" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"id\": 123,  \"category\": {    \"id\": 0,    \"name\": \"string\"  },  \"name\": \"doggie\",  \"photoUrls\": [    \"string\"  ],  \"tags\": [    {      \"id\": 0,      \"name\": \"string\"    }  ],  \"status\": \"available\"}"
     * This method fail creation pet by invalid Id type.
     *
     * @param petId ID of pet object. (Required)
     * @return 405 - Invalid input. ValidatableResponse object where response body contains an error with type/code/message
     */
    @Step("Create Pet by providing invalid string type petId: {0} via POST request")
    public ValidatableResponse createInvalidIdSuppliedPet(String petId) {
        PetInvalidIdSupplied pet = PetInvalidIdSupplied.builder().id(petId).build();
        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .body(pet)
                .post("/pet")
                .then()
                .log().all();
    }

    /**
     * PUT
     * curl -X PUT "https://petstore.swagger.io/v2/pet" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"id\": 122,  \"category\": {    \"id\": 0,    \"name\": \"string\"  },  \"name\": \"doggie\",  \"photoUrls\": [    \"string\"  ],  \"tags\": [    {      \"id\": 0,      \"name\": \"string\"    }  ],  \"status\": \"available\"}"
     * This method updates pet using created Pet object
     *
     * @return 200 - Undocumented. ValidatableResponse object where response body contains updated pet object or 400 - Invalid ID supplied or 404 - Pet not found or 405 - Validation exception
     * @see Pet
     */
    @Step("Update pet by new data via PUT request")
    public ValidatableResponse updatePet(int petId, String petName, int categoryId, String categoryName, String photoUrls, String tagIds, String tagNames, String petStatus) {
        Pet pet = createPetObject(petId, petName, categoryId, categoryName, photoUrls, tagIds, tagNames, petStatus);
        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .log().all()
                .body(pet)
                .put("/pet")
                .then()
                .log().all();
    }

    /**
     * PUT
     * curl -X POST "https://petstore.swagger.io/v2/pet" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"id\": 123,  \"category\": {    \"id\": 0,    \"name\": \"string\"  },  \"name\": \"doggie\",  \"photoUrls\": [    \"string\"  ],  \"tags\": [    {      \"id\": 0,      \"name\": \"string\"    }  ],  \"status\": \"available\"}"
     * This method try to create pet via invalid endpoint.
     *
     * @return 404 - Not found. ValidatableResponse object where response body contains an error with type/code/message
     * @see Pet
     */
    @Step("Update pet by new data via PUT request to invalid endpoint")
    public ValidatableResponse updatePetViaInvalidEndpoint(int petId, String petName, int categoryId, String categoryName, String photoUrls, String tagIds, String tagNames, String petStatus) {
        Pet pet = createPetObject(petId, petName, categoryId, categoryName, photoUrls, tagIds, tagNames, petStatus);
        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .log().all()
                .body(pet)
                .put("/pets")
                .then()
                .log().all();
    }

    /**
     * This method helps to verify correctness of Pet object updating/creating
     *
     * @param categoryId   Category of category in pet object (Optional)
     * @param categoryName Category Name of category in pet object (Optional)
     * @param photoUrls    List of photo urls of pet object (Optional)
     * @param tagIds       List of tag ids urls of tag in pet object (Optional)
     * @param tagNames     List of tag names urls of tag in pet object (Optional)
     * @param result       Pet object which should be compared with all fields in arguments
     * @see PetCategory
     * @see PetTags
     */
    @Step("Verify accordance of each field in Pet object")
    public void verifyPetFieldsCorrectness(int categoryId, String categoryName, String photoUrls, String tagIds, String tagNames, Pet result) {
        MatcherAssert.assertThat(result.getCategory().getId(), equalTo(categoryId));
        MatcherAssert.assertThat(result.getCategory().getName(), equalTo(categoryName));
        for (int i = 0; i < result.getTags().size(); i++) {
            PetTags tags = result.getTags().get(i);
            MatcherAssert.assertThat(tags.getId(), equalTo(Integer.parseInt(tagIds.split(";")[i])));
            MatcherAssert.assertThat(tags.getName(), equalTo(tagNames.split(";")[i]));
        }
        List<String> urls = new ArrayList<>(result.getPhotoUrls());
        for (int i = 0; i < urls.size(); i++) {
            MatcherAssert.assertThat(urls.get(i), equalTo(photoUrls.split(";")[i]));
        }
    }

    /**
     * POST
     * curl -X POST "https://petstore.swagger.io/v2/pet/123123" -H  "accept: application/json" -H  "Content-Type: application/x-www-form-urlencoded" -d "name=wsetweg&status=test"
     * This method updates pet object by id.
     *
     * @param petId     ID of pet object. (Required)
     * @param petName   Name of pet object (Optional)
     * @param petStatus Status of pet object. Any values : available, pending, sold, etc. (Optional)
     * @return 200 - Undocumented. ValidatableResponse object where response body consists of code/type/message or 405 - Invalid input
     */
    @Step("Update pet data by POST request: petId: {0}, petName: {1}, petStatus: {2}")
    public ValidatableResponse updatePetByPost(int petId, String petName, String petStatus) {
        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .header("Accept", "application/json")
                .header("Content-type", "application/x-www-form-urlencoded")
                .formParam("name", petName)
                .formParam("status", petStatus)
                .when()
                .log().all()
                .post("/pet/" + petId)
                .then()
                .log().all();
    }
}
