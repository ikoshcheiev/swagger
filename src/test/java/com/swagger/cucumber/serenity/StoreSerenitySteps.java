package com.swagger.cucumber.serenity;

import com.swagger.model.StoreData.Order;
import com.swagger.utils.ReuseableSpecifications;
import io.restassured.response.ValidatableResponse;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

/**
 * Class which describes Store endpoint interactions
 *
 * @author Ivan Koshcheiev "koshcheiev.vanya@gmail.com"
 * @version 1.0
 * <p>
 * Available types: POST | GET | DELETE
 * @see Order
 */
public class StoreSerenitySteps {

    /**
     * POST
     * curl -X POST "https://petstore.swagger.io/v2/store/order" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"id\": 1111,  \"petId\": 0,  \"quantity\": 0,  \"shipDate\": \"2020-12-22T23:05:54.492Z\",  \"status\": \"placed\",  \"complete\": true}"
     * This method creates order for purchased pet
     *
     * @param id       Id of Order (optional)
     * @param petId    Id of pet (required)
     * @param quantity Quantity of pets (optional)
     * @param shipDate Data of order shipping (optional)
     * @param status   Status of order (optional)
     * @param complete Completeness of order in boolean type (optional)
     * @return 200 - successful operation. Order objects as ValidatableResponse object or 400 - Invalid Order or 404 - Order not found
     * @see Order
     */
    @Step("Create new order with data: id:{0}, petId:{1}, quantity:{2}, shipDate:{3}, status:{4}, complete:{5}")
    public ValidatableResponse placeOrderForPet(int id, int petId, int quantity, String shipDate, String status, Boolean complete) {
        Order order = Order.builder().id(id).petId(petId).quantity(quantity).shipDate(shipDate).status(status).complete(complete).build();
        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .body(order)
                .log().all()
                .post("/store/order")
                .then()
                .log().all();
    }

    /**
     * GET
     * curl -X GET "https://petstore.swagger.io/v2/store/order/3" -H  "accept: application/json"
     * This method returns order by Id. For valid response try integer IDs with value >= 1 and <= 10. Other values will generated exceptions
     *
     * @param orderId Id of order (required)
     * @return 200 - successful operation. Order objects as ValidatableResponse object or 400 - Invalid ID supplied or 404 - Order not found
     * @see Order
     */
    @Step("Get order by orderID: {0}")
    public ValidatableResponse getOrderById(int orderId) {
        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .log().all()
                .get("/store/order/" + orderId)
                .then()
                .log().all();
    }

    /**
     * DELETE
     * curl -X DELETE "https://petstore.swagger.io/v2/store/order/1" -H  "accept: application/json"
     * This method deletes purchase order by ID. For valid response try integer IDs with positive integer value. Negative or non-integer values will generate API errors
     *
     * @param orderId Id of the order that needs to be deleted (required)
     * @return 200. ValidatableResponse object with code/type/message or 400 - Invalid ID supplied or 404 - Order not found
     */
    @Step("Remove order by orderID: {0}")
    public ValidatableResponse removeOrderById(int orderId) {
        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .log().all()
                .delete("/store/order/" + orderId)
                .then()
                .log().all();
    }

    /**
     * GET
     * curl -X GET "https://petstore.swagger.io/v2/store/inventory" -H  "accept: application/json"
     * This method returns a map of status codes to quantities
     *
     * @return 200 - successful operation. ValidatableResponse object with map of 'status':'quantity'
     */
    @Step("Get list of all used pet statuses")
    public ValidatableResponse getPetInventories() {
        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .log().all()
                .get("/store/inventory")
                .then()
                .log().all();
    }
}
