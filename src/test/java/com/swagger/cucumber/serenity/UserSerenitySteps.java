package com.swagger.cucumber.serenity;

import com.swagger.model.UserData.User;
import com.swagger.utils.ReuseableSpecifications;
import io.restassured.response.ValidatableResponse;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import java.util.ArrayList;
import java.util.List;

/**
 * Class which describes User endpoint interactions
 *
 * @author Ivan Koshcheiev "koshcheiev.vanya@gmail.com"
 * @version 1.0
 * <p>
 * Available types: POST | PUT | GET | DELETE
 * @see User
 */
public class UserSerenitySteps {

    /**
     * POST
     * curl -X POST "https://petstore.swagger.io/v2/user" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"id\": 120,  \"username\": \"string\",  \"firstName\": \"string\",  \"lastName\": \"string\",  \"email\": \"string\",  \"password\": \"string\",  \"phone\": \"string\",  \"userStatus\": 0}"
     * This method creates new user. This can only be done by the logged in user.
     *
     * @param id         Id of new user (required)
     * @param userName   Name of new user (optional)
     * @param firstName  First name of new user (optional)
     * @param lastName   Last name of new user (optional)
     * @param email      Email address of new user (optional)
     * @param password   Password of new user (optional)
     * @param phone      Ph0ne of new user (optional)
     * @param userStatus Status of new user (optional)
     * @return 200 - Successful operation. Returns map object with keys code/type/message.
     * @see User
     */
    @Step("Create user with information: id:{0}, userName:{1}, firstName:{2}, lastName:{3}, email:{4}, password:{5}, phone:{6}, userStatus:{7}")
    public ValidatableResponse createUser(int id, String userName, String firstName, String lastName,
                                          String email, String password, String phone, int userStatus) {
        User user = User.builder().id(id).username(userName).firstName(firstName).lastName(lastName).email(email).password(password).phone(phone).userStatus(userStatus).build();
        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .body(user)
                .log().all()
                .post("/user")
                .then()
                .log().all();
    }

    /**
     * POST
     * curl -X POST "https://petstore.swagger.io/v2/user/createWithList" -H  "accept: application/json" -H  "Content-Type: application/json" -d "[  {    \"id\": 1,    \"username\": \"string\",    \"firstName\": \"string\",    \"lastName\": \"string\",    \"email\": \"string\",    \"password\": \"string\",    \"phone\": \"string\",    \"userStatus\": 0  },  {    \"id\": 2,    \"username\": \"string\",    \"firstName\": \"string\",    \"lastName\": \"string\",    \"email\": \"string\",    \"password\": \"string\",    \"phone\": \"string\",    \"userStatus\": 0  }]"
     * This method creates new user or list of users.
     *
     * @param id         Id of new user (required)
     * @param userName   Name of new user (optional)
     * @param firstName  First name of new user (optional)
     * @param lastName   Last name of new user (optional)
     * @param email      Email address of new user (optional)
     * @param password   Password of new user (optional)
     * @param phone      Ph0ne of new user (optional)
     * @param userStatus Status of new user (optional)
     * @return 200 - Successful operation. Returns map object with keys code/type/message.
     * @see User
     */
    @Step("Creating User with all data: id:{0}, userName:{1}, firstName:{2}, lastName:{3}, email:{4}, password:{5}, phone:{6}, userStatus:{7}")
    public ValidatableResponse createUserWithList(String id, String userName, String firstName, String lastName,
                                                  String email, String password, String phone, String userStatus) {
        List<User> users = new ArrayList<>();
        for (int i = 0; i < id.split(";").length; i++) {
            int userid = Integer.parseInt(id.split(";")[i]);
            String username = userName.split(";")[i];
            String userfirstname = firstName.split(";")[i];
            String userlastname = lastName.split(";")[i];
            String useremail = email.split(";")[i];
            String userpassword = password.split(";")[i];
            String userphone = phone.split(";")[i];
            int userstatus = Integer.parseInt(userStatus.split(";")[i]);
            users.add(new User(userid, username, userfirstname, userlastname, useremail, userpassword, userphone, userstatus));
        }

        return createUsersWithArrays(users);
    }


    /**
     * POST
     * curl -X POST "https://petstore.swagger.io/v2/user/createWithArray" -H  "accept: application/json" -H  "Content-Type: application/json" -d "[  {    \"id\": 3,    \"username\": \"string\",    \"firstName\": \"string\",    \"lastName\": \"string\",    \"email\": \"string\",    \"password\": \"string\",    \"phone\": \"string\",    \"userStatus\": 0  },  {    \"id\": 2,    \"username\": \"string\",    \"firstName\": \"string\",    \"lastName\": \"string\",    \"email\": \"string\",    \"password\": \"string\",    \"phone\": \"string\",    \"userStatus\": 0  }]"
     * This method creates new user or array of users.
     *
     * @param id         Id of new user (required)
     * @param userName   Name of new user (optional)
     * @param firstName  First name of new user (optional)
     * @param lastName   Last name of new user (optional)
     * @param email      Email address of new user (optional)
     * @param password   Password of new user (optional)
     * @param phone      Ph0ne of new user (optional)
     * @param userStatus Status of new user (optional)
     * @return 200 - Successful operation. Returns map object with keys code/type/message.
     * @see User
     */
    @Step("Creating User with all data: id:{0}, userName:{1}, firstName:{2}, lastName:{3}, email:{4}, password:{5}, phone:{6}, userStatus:{7}")
    public ValidatableResponse createUserWithArray(String id, String userName, String firstName, String lastName,
                                                   String email, String password, String phone, String userStatus) {
        User[] users = new User[id.split(";").length];
        for (int i = 0; i < id.split(";").length; i++) {
            int userid = Integer.parseInt(id.split(";")[i]);
            String username = userName.split(";")[i];
            String userfirstname = firstName.split(";")[i];
            String userlastname = lastName.split(";")[i];
            String useremail = email.split(";")[i];
            String userpassword = password.split(";")[i];
            String userphone = phone.split(";")[i];
            int userstatus = Integer.parseInt(userStatus.split(";")[i]);
            users[i] = new User(userid, username, userfirstname, userlastname, useremail, userpassword, userphone, userstatus);
        }

        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .body(users)
                .log().all()
                .post("/user/createWithArray")
                .then()
                .log().all();
    }

    /**
     * POST
     * curl -X POST "https://petstore.swagger.io/v2/user/createWithArray" -H  "accept: application/json" -H  "Content-Type: application/json" -d "[  {    \"id\": 3,    \"username\": \"string\",    \"firstName\": \"string\",    \"lastName\": \"string\",    \"email\": \"string\",    \"password\": \"string\",    \"phone\": \"string\",    \"userStatus\": 0  },  {    \"id\": 2,    \"username\": \"string\",    \"firstName\": \"string\",    \"lastName\": \"string\",    \"email\": \"string\",    \"password\": \"string\",    \"phone\": \"string\",    \"userStatus\": 0  }]"
     * This method creates new user or array of users only by Id.
     *
     * @param userName Name of new user (required)
     * @param password Password of new user (required)
     * @return 200 - Successful operation. Returns map object with keys code/type/message.
     * @see User
     */
    @Step("Creating User only by ID:{0} and Name:{1}")
    public ValidatableResponse createUserWithArray(String userName, String password) {
        User user = User.builder().username(userName).password(password).build();
        List<User> usersData = new ArrayList<>();
        usersData.add(user);
        return createUsersWithArrays(usersData);
    }

    /**
     * Helper method for creation users by List
     */
    @Step("Creating Users by List with users' data")
    private ValidatableResponse createUsersWithArrays(List<User> users) {
        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .body(users)
                .log().all()
                .post("/user/createWithArray")
                .then()
                .log().all();
    }

    /**
     * GET
     * curl -X GET "https://petstore.swagger.io/v2/user/user1" -H  "accept: application/json"
     * This method returns User object by name. The name that needs to be fetched. Use user1 for testing.
     *
     * @param userName Name of new user (required)
     * @return 200 - Successful operation. Returns User object. or 400 Invalid username supplied or 404 User not found
     * @see User
     */
    @Step("Get user info by name: {0}")
    public ValidatableResponse getUserByName(String userName) {
        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .log().all()
                .get("/user/" + userName)
                .then()
                .log().all();
    }

    /**
     * DELETE
     * curl -X DELETE "https://petstore.swagger.io/v2/user/1" -H  "accept: application/json"
     * This method removes user by user name. This can only be done by the logged in user.
     *
     * @param userName Name of new user (required)
     * @return 200 - Successful operation. Returns map object with keys code/type/message. or 400 Invalid username supplied or 404 User not found
     */
    @Step("Deleting user information with name: {0}")
    public ValidatableResponse deleteUser(String userName) {
        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .log().all()
                .delete("/user/" + userName)
                .then()
                .log().all();
    }

    /**
     * PUT
     * curl -X PUT "https://petstore.swagger.io/v2/user/username" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"id\": 123,  \"username\": \"string\",  \"firstName\": \"string\",  \"lastName\": \"string\",  \"email\": \"string\",  \"password\": \"string\",  \"phone\": \"string\",  \"userStatus\": 0}"
     * This method updates existing user. This can only be done by the logged in user.
     *
     * @param id         Id of new user (required)
     * @param userName   Name of new user (optional)
     * @param firstName  First name of new user (optional)
     * @param lastName   Last name of new user (optional)
     * @param email      Email address of new user (optional)
     * @param password   Password of new user (optional)
     * @param phone      Ph0ne of new user (optional)
     * @param userStatus Status of new user (optional)
     * @return 200 - Successful operation. Returns map object with keys code/type/message. or 400 Invalid user supplied or 404 User not found
     * @see User
     */
    @Step("Updating user data by: id:{0}, userName:{1}, firstName:{2}, lastName:{3}, email:{4}, password:{5}, phone:{6}, userStatus:{7}")
    public ValidatableResponse updateUserByPut(int id, String userName, String firstName, String lastName, String email, String password, String phone, int userStatus) {
        User user = new User(id, userName, firstName, lastName, email, password, phone, userStatus);

        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .body(user)
                .log().all()
                .put("/user/" + userName)
                .then()
                .log().all();
    }

    /**
     * GET
     * curl -X GET "https://petstore.swagger.io/v2/user/login?username=username&password=123" -H  "accept: application/json"
     * This method logs user in.
     *
     * @param userName Name of new user (required)
     * @param password Password of new user (required)
     * @return 200 - successful operation. Returns map object with keys code/type/message. 400 Invalid username/password supplied
     */
    @Step("logging user in by: user name:{0}, password:{1}")
    public ValidatableResponse logInUserByGet(String userName, String password) {
        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .header("Accept", "application/json")
                .queryParam("username", userName)
                .queryParam("password", password)
                .when()
                .log().all()
                .get("/user/login")
                .then()
                .log().all();
    }

    /**
     * GET
     * curl -X GET "https://petstore.swagger.io/v2/user/logout" -H  "accept: application/json"
     * This method logs all user outs.
     *
     * @return 200 - successful operation. Returns map object with keys code/type/message.
     */
    @Step("logging out all users")
    public ValidatableResponse logOutUserByGet() {
        return SerenityRest.rest().given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .log().all()
                .get("/user/logout")
                .then()
                .log().all();
    }
}
