package com.swagger.cucumber.steps;

import com.swagger.cucumber.serenity.PetSerenitySteps;
import com.swagger.model.PetData.Pet;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.apache.http.HttpStatus;

import java.util.List;

import static org.hamcrest.Matchers.*;

/**
 * Class for describing steps of features
 */
public class PetSteps {

    @Steps
    PetSerenitySteps steps;

    @When("^User sends a GET requests to the pet endpoint, it must get back the list of pets in appropriate status$")
    public void userSendsAGetRequestToThePetEndpoint(List<String> statuses) {
        for (String status : statuses) {
            steps.getPetsByStatus(status)
                    .assertThat()
                    .statusCode(HttpStatus.SC_OK);
        }
    }

    @When("^User sends a GET with invalid status \"([^\"]*)\", it must get back an \"([^\"]*)\" \"([^\"]*)\" code$")
    public void userSendsAGetWithInvalidStatus(String invalid_status, int httpStatus, String codeDescription) {
        steps.getPetsByStatus(invalid_status)
                .assertThat()
                .statusCode(httpStatus)
                .statusLine(containsString(codeDescription));
    }

    @When("^User sends a POST with petId (.*) for uploading an image (.*) and Addition Metadata (.*), it must receive code (.*)$")
    public void userSendsAPostWithPetIdIdForUploadingAnImage(int petId, String fileName, String additionalMetadata, int httpCode) {
        steps.uploadImage(petId, fileName, additionalMetadata)
                .assertThat()
                .statusCode(httpCode)
                .body("message", containsString(fileName))
                .body("message", containsString("additionalMetadata: " + additionalMetadata));
    }

    @Given("^Remove pet id (.*) if existing$")
    public void removePetIdIfExists(int petId) {
        if (steps.getPetById(petId).extract().statusCode() == 200) {
            steps.removePetById(petId)
                    .assertThat()
                    .statusCode(HttpStatus.SC_OK)
                    .body("", hasEntry("type", "unknown"))
                    .body("", hasEntry("message", Integer.toString(petId)));
        }
    }

    @Given("^Create pet if not existing or update existing by data: (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*)$")
    public void createPetIfNotExisting(int petId, String petName, int categoryId, String categoryName, String photoUrls,
                                       String tagIds, String tagNames, String petStatus) {
        steps.createPet(petId, petName, categoryId, categoryName, photoUrls, tagIds, tagNames, petStatus)
                .assertThat()
                .statusCode(HttpStatus.SC_OK);
    }

    @Given("^Create pet by id (.*) if not existing$")
    public void createPetIdIfNotExisting(int petId) {
        if (steps.getPetById(petId).extract().statusCode() != 200) {
            steps.createPet(petId)
                    .assertThat()
                    .statusCode(HttpStatus.SC_OK);
        }
    }

    @When("^User sends a POST for creating new Pet by providing the information (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*)$")
    public void userSendsAPOSTForCreatingNewPet(int petId, String petName, int categoryId, String categoryName,
                                                String photoUrls, String tagIds, String tagNames, String petStatus) {
        Pet result = steps.createPet(petId, petName, categoryId, categoryName, photoUrls, tagIds, tagNames, petStatus)
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("name", containsString(petName))
                .body("status", containsString(petStatus))
                .extract().as(Pet.class);

        steps.verifyPetFieldsCorrectness(categoryId, categoryName, photoUrls, tagIds, tagNames, result);
    }


    @When("^User sends a GET to pet endpoint with not existing id (.*), it must receive (.*) with text \"([^\"]*)\"$")
    public void userSendsGetToPetEndpointWithNotExistingId(int petId, int httpCode, String codeDescription) {
        steps.getPetById(petId)
                .assertThat()
                .statusCode(httpCode)
                .body("message", equalTo(codeDescription));
    }

    @Then("^Verify that the pet with (.*) is absent and an error appears (.*), (.*)$")
    public void verifyThatThePetWithPetIdIsAbsent(int petId, int httpCode, String codeDescription) {
        steps.getPetById(petId)
                .assertThat()
                .statusCode(httpCode)
                .body("message", equalTo(codeDescription));
    }

    @When("^User sends a PUT for updating existing Pet by providing the information (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*) and status code (\\d+), \"([^\"]*)\"$")
    public void userSendsAPUTForUpdatingExistingPet(int petId, String petName, int categoryId, String categoryName,
                                                    String photoUrls, String tagIds, String tagNames, String petStatus, int httpCode, String codeDescription) {
        steps.updatePet(petId, petName, categoryId, categoryName, photoUrls, tagIds, tagNames, petStatus)
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .statusLine(containsString(codeDescription));
    }

    @When("^User sends a PUT to endpoint for updating existing Pet by providing the information (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*) and status code (.*), (.*) received$")
    public void userSendsAPUTToEndpointForUpdatingExistingPet(int petId, String petName, int categoryId, String categoryName,
                                                              String photoUrls, String tagIds, String tagNames, String petStatus, int httpCode, String codeDescription) {
        steps.updatePet(petId, petName, categoryId, categoryName, photoUrls, tagIds, tagNames, petStatus)
                .statusCode(httpCode)
                .statusLine(containsString(codeDescription));

    }

    @When("^User sends a PUT to invalid endpoint for updating existing Pet by providing the information (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*) and status code (.*), (.*)$")
    public void userSendsAPUTToInvalidEndpoint(int petId, String petName, int categoryId, String categoryName,
                                               String photoUrls, String tagIds, String tagNames, String petStatus, int httpCode, String codeDescription) {
        steps.updatePetViaInvalidEndpoint(petId, petName, categoryId, categoryName, photoUrls, tagIds, tagNames, petStatus)
                .statusCode(httpCode)
                .statusLine(containsString(codeDescription));
    }

    @When("^User sends a POST to endpoint for creating new Pet with invalid String id \"([^\"]*)\", it must get back error code \"([^\"]*)\", \"([^\"]*)\"$")
    public void userSendsAPOSTToEndpointForCreatingNewPetWithInvalidStringtId(String id, int httpCode, String codeDescription) {
        steps.createInvalidIdSuppliedPet(id)
                .statusCode(httpCode)
                .statusLine(containsString(codeDescription));
    }

    @When("^User sends a GET to endpoint for receiving Pet by providing tags information (.*), it must receive code (.*), \"([^\"]*)\"$")
    public void userSendsAGETToEndpointForByTags(List<String> tagNames, int httpCode, String codeDescription) {
        steps.getPetByTags(tagNames)
                .statusCode(httpCode)
                .statusLine(containsString(codeDescription));
    }

    @When("^User sends a GET to endpoint for receiving Pet by providing Id information (.*), it must receive proper data in Pet object (.*), (.*), (.*), (.*), (.*), (.*), (.*)$")
    public void userSendsAGETToEndpointForReceivingById(int petId, String petName, int categoryId, String categoryName,
                                                        String photoUrls, String tagIds, String tagNames, String petStatus) {
        Pet result = steps.getPetById(petId)
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("name", containsString(petName))
                .body("status", containsString(petStatus))
                .extract().as(Pet.class);
        steps.verifyPetFieldsCorrectness(categoryId, categoryName, photoUrls, tagIds, tagNames, result);
    }

    @When("^User sends a POST for updating Pet by providing the information (.*), (.*), (.*) and status code (.*), \"([^\"]*)\"$")
    public void userSendsAPOSTForUpdatingPet(int petId, String petName, String petStatus, int httpCode, String codeDescription) {
        steps.updatePetByPost(petId, petName, petStatus)
                .assertThat()
                .statusCode(httpCode)
                .statusLine(containsString(codeDescription))
                .body("code", equalTo(httpCode))
                .body("message", containsString(String.valueOf(petId)));
    }

    @Then("^Verify that the pet with (.*) is created$")
    public void verifyThatThePetWithPetIdIsCreated(int petId) {
        steps.getPetById(petId)
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("id", equalTo(petId));
    }

    @Then("^Verify that the pet with (.*) is present and has correct data (.*), (.*)$")
    public void verifyThatThePetWithPetIdIsPresent(int petId, String petName, String petStatus) {
        steps.getPetById(petId)
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("name", containsString(petName))
                .body("status", containsString(petStatus));
    }

    @Then("^Verify the pet with (.*), (.*), (.*) is present and has correct data (.*), (.*), (.*), (.*), (.*)$")
    public void verifyThePetWithPetIdIsPresentAndHasCorrectData(int petId, String petName, String petStatus, int categoryId,
                                                                String categoryName, String photoUrls, String tagIds, String tagNames) {
        Pet result = steps.getPetById(petId)
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("name", containsString(petName))
                .body("status", containsString(petStatus))
                .extract().as(Pet.class);

        steps.verifyPetFieldsCorrectness(categoryId, categoryName, photoUrls, tagIds, tagNames, result);
    }

    @When("^User sends DELETE for removing pet by (\\d+), it must receive code (\\d+), \"([^\"]*)\"$")
    public void userSendsDELETEForRemovingPetByItMustReceiveCode(int petId, int httpCode, String CodeDescription) {
        if (steps.getPetById(petId).extract().statusCode() == 200) {
            steps.removePetById(petId)
                    .assertThat()
                    .statusCode(httpCode)
                    .statusLine(containsString(CodeDescription))
                    .body("", hasEntry("type", "unknown"))
                    .body("", hasEntry("message", Integer.toString(petId)));
        } else
            steps.removePetById(petId)
                    .assertThat()
                    .statusCode(httpCode)
                    .statusLine(containsString(CodeDescription));
    }
}