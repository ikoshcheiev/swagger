package com.swagger.cucumber.steps;

import com.swagger.cucumber.serenity.PetSerenitySteps;
import com.swagger.cucumber.serenity.StoreSerenitySteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.formatter.model.Match;
import io.restassured.response.ValidatableResponse;
import net.thucydides.core.annotations.Steps;
import org.apache.http.HttpStatus;
import org.hamcrest.Matcher;
import org.hamcrest.MatcherAssert;
import org.junit.Assert;

import java.util.Map;

import static org.hamcrest.Matchers.*;

/**
 * Class for describing steps of features
 */
public class StoreSteps {
    private Map<String, Integer> petInventories;

    @Steps
    StoreSerenitySteps steps;
    @Steps
    PetSerenitySteps petsteps;

    @When("^User sends a POST request for placing order with all data: (.*), (.*), (.*), (.*), (.*), (.*), it must get back (.*) and \"([^\"]*)\"$")
    public void userSendsAPOSTRequestForPlacingOrder(int orderId, int petId, int quantity, String shipDate, String status,
                                                     Boolean complete, int httpCode, String CodeDescription) {
        steps.placeOrderForPet(orderId, petId, quantity, shipDate, status, complete)
                .statusCode(httpCode)
                .statusLine(containsString(CodeDescription))
                .body("", hasEntry("id", orderId))
                .body("", hasEntry("petId", petId))
                .body("", hasEntry("quantity", quantity))
                .body("", hasEntry("shipDate", shipDate))
                .body("", hasEntry("status", status))
                .body("", hasEntry("complete", complete));
    }

    @Given("^Create order if not exists with all data: (.*), (.*), (.*), (.*), (.*), (.*), it must get back (.*)$")
    public void createOrderIfNotExists(int orderId, int petId, int quantity, String shipDate, String status,
                                       Boolean complete, int httpCode) {
        if (steps.getOrderById(orderId).extract().statusCode() != 200) {
            steps.placeOrderForPet(orderId, petId, quantity, shipDate, status, complete)
                    .statusCode(httpCode);
        }
    }

    @Then("^Remove order if exists by ID (.*)$")
    public void removeOrderIfExistsByID(int orderId) {
        if (steps.getOrderById(orderId).extract().statusCode() == 200) {
            steps.removeOrderById(orderId)
                    .statusCode(HttpStatus.SC_OK)
                    .body("", hasEntry("type", "unknown"))
                    .body("", hasEntry("message", String.valueOf(orderId)));//?
        }
    }

    @When("^User sends a Delete for removing order by order ID: (.*), it must get back (.*) and if order wasn't existing then (.*) \"([^\"]*)\" or (.*) \"([^\"]*)\"$")
    public void userSendsADeleteForRemovingOrderByOrderIDId(int orderId, int successCode,
                                                            int notFoundCode, String notFoundMessage,
                                                            int invalidCode, String invalidMessage) {
        if (steps.getOrderById(orderId).extract().statusCode() == 200) {
            steps.removeOrderById(orderId)
                    .statusCode(successCode)
                    .body("", hasEntry("type", "unknown"))
                    .body("message", equalTo(String.valueOf(orderId)));
        } else if (orderId < 1) {
            steps.removeOrderById(orderId)
                    .statusCode(invalidCode)
                    .body("", hasEntry("type", "unknown"))
                    .body("", hasEntry("message", invalidMessage));
        } else {
            steps.removeOrderById(orderId)
                    .statusCode(notFoundCode)
                    .body("", hasEntry("type", "unknown"))
                    .body("", hasEntry("message", notFoundMessage));
        }
    }


    @When("^User sends a GET for finding purchased order by Id:(.*), it must get back (.*) and contain all proper data: (.*), (.*), (.*), (.*), (.*)$")
    public void userSendsAGETForFindingPurchasedOrderByIdId(int id, int httpCode, int petId, int quantity, String shipDate,
                                                            String status, Boolean complete) {
        steps.getOrderById(id)
                .statusCode(httpCode)
                .body("", hasEntry("id", id))
                .body("", hasEntry("petId", petId))
                .body("", hasEntry("quantity", quantity))
                .body("", hasEntry("shipDate", shipDate))
                .body("", hasEntry("status", status))
                .body("", hasEntry("complete", complete));
    }

    @When("^User sends a GET for fail finding removed purchased order by Id:(.*), it must get back http code (.*), message \"([^\"]*)\", type (.*)$")
    public void userSendsAGETForFailFindingRemovedPurchasedOrderById(int id, int httpCode, String message, String type) {
        steps.getOrderById(id)
                .statusCode(httpCode)
                .body("", hasEntry("type", type))
                .body("", hasEntry("message", message));
    }

    @And("^User sends a GET requests for receiving pet inventories, it must get back (.*) and pet status is present (.*)$")
    public void userSendsAGETRequestsForReceivingPetInventories(int successCode, String petStatus) {
        ValidatableResponse inventories = steps.getPetInventories()
                .statusCode(successCode);
        if (petInventories != null) {
            Map<String, Integer> petInventoriesTemporary = inventories.extract().body().jsonPath().getMap("$");
            if (petInventoriesTemporary.get(petStatus) != null && petInventoriesTemporary.get(petStatus) > 1)
                MatcherAssert.assertThat(petInventoriesTemporary.get(petStatus) - 1, equalTo(petInventories.get(petStatus)));
            else if (petInventoriesTemporary.get(petStatus) == 1) {
                MatcherAssert.assertThat(petInventories.get(petStatus), is(nullValue()));
            }
        }
        petInventories = inventories.extract().body().jsonPath().getMap("$");
    }
}
