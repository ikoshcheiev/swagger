package com.swagger.cucumber.steps;

import com.swagger.cucumber.serenity.UserSerenitySteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.ValidatableResponse;
import net.thucydides.core.annotations.Steps;
import org.apache.http.HttpStatus;
import org.hamcrest.MatcherAssert;

import java.util.Date;

import static org.hamcrest.Matchers.*;

/**
 * Class for describing steps of features
 */
public class UserSteps {

    @Steps
    UserSerenitySteps steps;

    @Given("^User with data (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*) is created in the swagger application$")
    public void userWithDataIsCreated(int userId, String userName, String firstName, String lastName,
                                      String email, String password, String phone, int userStatus) {
        if (steps.getUserByName(userName).extract().statusCode() != 200) {
            steps.createUser(userId, userName, firstName, lastName, email, password, phone, userStatus)
                    .assertThat()
                    .statusCode(HttpStatus.SC_OK);
        }
    }

    @When("^User sends a GET request to the user endpoint by (.*), it must get back userdata (.*), (.*), (.*), (.*), (.*), (.*), (.*), it must get back (.*)$")
    public void userSendsAGETRequestToTheUserEndpoint(String userName, int userId, String firstName, String lastName,
                                                      String email, String password, String phone, int userStatus, int httpCode) {
        steps.getUserByName(userName)
                .assertThat()
                .statusCode(httpCode)
                .body("", hasEntry("id", userId))
                .body("", hasEntry("username", userName))
                .body("", hasEntry("firstName", firstName))
                .body("", hasEntry("lastName", lastName))
                .body("", hasEntry("email", email))
                .body("", hasEntry("password", password))
                .body("", hasEntry("phone", phone))
                .body("", hasEntry("userStatus", userStatus));
    }

    @When("^User sends a GET request to not existing user endpoint by (.*), it must get back error (.*) and \"([^\"]*)\" message$")
    public void userSendsAGETRequestToTheUserEndpoint(String userName, int httpCode, String errorMessage) {
        steps.getUserByName(userName)
                .assertThat()
                .statusCode(httpCode)
                .body("message", containsString(errorMessage));
    }

    @Given("^User with data (.*) is removed from the swagger application$")
    public void userWithDataUserNameIsRemoved(String userNameList) {
        String[] users = userNameList.split(";").clone();
        for (String userName : users) {
            if (steps.getUserByName(userName).extract().statusCode() == 200) {
                steps.deleteUser(userName)
                        .assertThat()
                        .statusCode(HttpStatus.SC_OK)
                        .body("message", containsString(userName));
            }
        }
    }

    @Then("^Users with appropriate data created/updated (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*)$")
    public void UsersWithAppropriateDataCreatedUpdated(String userId, String userName, String firstName, String lastName,
                                                       String email, String password, String phone, String userStatus) {
        for (int i = 0; i < userId.split(";").length; i++) {
            steps.getUserByName(userName.split(";")[i])
                    .assertThat()
                    .statusCode(HttpStatus.SC_OK)
                    .body("id", equalTo(Integer.parseInt(userId.split(";")[i])))
                    .body("firstName", containsString(firstName.split(";")[i]))
                    .body("lastName", containsString(lastName.split(";")[i]))
                    .body("email", containsString(email.split(";")[i]))
                    .body("password", containsString(password.split(";")[i]))
                    .body("phone", containsString(phone.split(";")[i]))
                    .body("userStatus", equalTo(Integer.parseInt(userStatus.split(";")[i])));
        }
    }

    @When("^User sends a POST request for creation/updating new user with list of User's data: (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*); it must get back (.*)$")
    public void userSendsAPOSTRequestForCreationNewUser(String userId, String userName, String firstName, String lastName,
                                                        String email, String password, String phone, String userStatus, int httpCode) {
        steps.createUserWithList(userId, userName, firstName, lastName, email,
                password, phone, userStatus)
                .assertThat()
                .statusCode(httpCode)
                .body("message", containsString("ok"));
    }

    @When("^User sends a POST request for creation new user with array of User's data: (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*); it must get back (.*)$")
    public void userSendsAPOSTRequestForCreationNewUserWithArrayOfUser(String userId, String userName, String firstName, String lastName,
                                                                       String email, String password, String phone, String userStatus, int httpCode) {
        steps.createUserWithArray(userId, userName, firstName, lastName, email,
                password, phone, userStatus)
                .assertThat()
                .statusCode(httpCode)
                .body("message", containsString("ok"));
    }

    @When("^User sends a POST request for creation new user with data: (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), it must get back (.*)$")
    public void userSendsAPOSTRequestForCreationNewUser(int userId, String userName, String firstName, String lastName,
                                                        String email, String password, String phone, int userStatus, int httpCode) {
        steps.createUser(userId, userName, firstName, lastName, email,
                password, phone, userStatus)
                .assertThat()
                .statusCode(httpCode)
                .body("", hasEntry("message", Integer.toString(userId)));
    }

    @When("^User sends a PUT request for updating user with data: (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*), it must get back (.*)$")
    public void userSendsAPUTRequestForUpdatingUser(int userId, String userName, String firstName, String lastName,
                                                    String email, String password, String phone, int userStatus, int httpCode) {
        steps.updateUserByPut(userId, userName, firstName, lastName, email, password, phone, userStatus)
                .assertThat()
                .statusCode(httpCode)
                .body("", hasEntry("message", Integer.toString(userId)));
    }

    @When("^User sends a DELETE request for removing user with (.*) from the swagger application, it must receive code (.*), \"([^\"]*)\"$")
    public void userSendsADELETERequestForRemovingUser(String userName, int httpCode, String httpDescription) {

        if (steps.getUserByName(userName).extract().statusCode() == 200) {
            steps.deleteUser(userName)
                    .assertThat()
                    .statusCode(httpCode)
                    .statusLine(containsString(httpDescription))
                    .body("", hasEntry("type", "unknown"))
                    .body("", hasEntry("message", userName));
        } else
            steps.deleteUser(userName)
                    .assertThat()
                    .statusCode(httpCode)
                    .statusLine(containsString(httpDescription));
    }

    @When("^User sends a GET request for logging in to the user endpoint by login:(.*), password:(.*), it must get back message \"([^\"]*)\" with timestamp at the end$")
    public void userSendsAGETRequestForValidLoggingIn(String userName, String password, String message) {
        ValidatableResponse resp = getValidatableResponseOnLogIn(userName, password).assertThat().statusCode(HttpStatus.SC_OK);
        long dateTimeMillis = new Date(resp.extract().header("date")).getTime();
        MatcherAssert.assertThat(
                resp.extract().body().jsonPath().get("message"),
                containsString((message + dateTimeMillis / 1000)));
        MatcherAssert.assertThat(new Date(resp.extract().header("x-expires-after")).getTime(),
                equalTo(dateTimeMillis + 60 * 60 * 1000));
    }

    @And("^User sends a GET request for logging out to the user endpoint, it must get back (.*)$")
    public void userSendsAGETRequestForLoggingOut(int httpCode) {
        steps.logOutUserByGet()
                .assertThat()
                .statusCode(httpCode);
    }

    @And("^User sends a GET request for logging in to the user endpoint by (.*) and (.*)$")
    public void userSendsAGETRequestForLoggingInToTheUserEndpoint(String userName, String password) {
        getValidatableResponseOnLogIn(userName, password)
                .assertThat()
                .statusCode(HttpStatus.SC_OK);
    }

    private ValidatableResponse getValidatableResponseOnLogIn(String userName, String password) {
        return steps.logInUserByGet(userName, password);
    }
}
