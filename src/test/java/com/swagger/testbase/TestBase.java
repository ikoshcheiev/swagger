package com.swagger.testbase;

import io.restassured.RestAssured;
import org.junit.BeforeClass;

/**
 * Base configurations needed for running tests on endpoints like RestAssured.baseURI.
 * Extended by cucumber runners
 *
 * @see com.swagger.cucumber.PetRunner
 * @see com.swagger.cucumber.OrderRunner
 * @see com.swagger.cucumber.UserRunner
 */
public class TestBase {

    @BeforeClass
    public static void init() {
        RestAssured.baseURI = "https://petstore.swagger.io/v2";
    }
}
