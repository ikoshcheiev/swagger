package com.swagger.utils;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.lessThan;

/**
 * Reusable specification for generation base of api requests/responses
 */
public class ReuseableSpecifications {

    public static RequestSpecBuilder rspec;
    public static RequestSpecification requestSpecification;

    public static ResponseSpecBuilder respec;
    public static ResponseSpecification responseSpecification;


    public static RequestSpecification getGenericRequestSpec() {
        rspec = new RequestSpecBuilder();
        rspec.setBaseUri("https://petstore.swagger.io");
        rspec.setBasePath("/v2");
        rspec.setContentType(ContentType.JSON);
        requestSpecification = rspec.build();
        return requestSpecification;

    }

    public static ResponseSpecification getGenericResponseSpec() {
        respec = new ResponseSpecBuilder();
        respec.expectHeader("Content-Type", "application/json");
        respec.expectResponseTime(lessThan(5L), TimeUnit.SECONDS);
        responseSpecification = respec.build();
        return responseSpecification;

    }
}
