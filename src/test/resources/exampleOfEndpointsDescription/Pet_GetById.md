**Find pet by ID**
----
Returns a single pet.

* **URL**

  /pet/{petId}

* **Method:**

  `GET`

*  **URL Params**

   **Required:**

   `id=[integer($int64)]`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 <br />
      **Content:** `{ descritpion : "successful operation" }`
      `````{
      "id": 0,
      "category": {
        "id": 0,
        "name": "string"
      },
      "name": "doggie",
      "photoUrls": [
        "string"
      ],
      "tags": [
        {
            "id": 0,
            "name": "string"
        }
      ],
      "status": "available"
      }

* **Error Response:**

    * **Code:** 400 <br />
      **Content:** `{ error : "Invalid ID supplied" }`

  OR

    * **Code:** 404 <br />
      **Content:** `{ error : "Pet not found" }`

* **Sample Call:**

  `curl -X GET "https://petstore.swagger.io/v2/pet/4" -H  "accept: application/json`

* **Notes:**

  None