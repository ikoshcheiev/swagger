**Find Pets by status**
----
Returns a single pet.

* **URL**

  /pet/findByStatus

* **Method:**

  `GET`

*  **URL Params**

   **Required:**
   
    `status=[array[string]]`

   **Available values** : [available, pending, sold]

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 <br />
      **Content:** `{ descritpion : "successful operation" }`
      ````{
      "id": 0,
      "category": {
        "id": 0,
        "name": "string"
      },
      "name": "doggie",
      "photoUrls": [
        "string"
      ],
      "tags": [
        {
            "id": 0,
            "name": "string"
        }
      ],
      "status": "available"
      }

* **Error Response:**

    * **Code:** 400 <br />
      **Content:** `{ error : "Invalid status value" }`

* **Sample Call:**

  `curl -X GET "https://petstore.swagger.io/v2/pet/findByStatus?status=available" -H  "accept: application/json"`

* **Notes:**

  None