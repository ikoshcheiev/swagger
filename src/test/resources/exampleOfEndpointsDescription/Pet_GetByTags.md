**Finds pet by ID**
----

_Warning: Deprecated_

Returns a single pet.

* **URL**

  /pet/findByTags

* **Method:**

  `GET`

*  **URL Params**

   **Required:**

   `tags=[array[string]]`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 <br />
      **Content:** `{ descritpion : "successful operation" }`
      `````{
      "id": 0,
      "category": {
        "id": 0,
        "name": "string"
      },
      "name": "doggie",
      "photoUrls": [
        "string"
      ],
      "tags": [
        {
            "id": 0,
            "name": "string"
        }
      ],
      "status": "available"
      }

* **Error Response:**

    * **Code:** 400 <br />
      **Content:** `{ error : "Invalid tag value" }`

* **Sample Call:**

  `curl -X GET "https://petstore.swagger.io/v2/pet/findByTags?tags=string1&tags=string2" -H  "accept: application/json"`

* **Notes:**

  None