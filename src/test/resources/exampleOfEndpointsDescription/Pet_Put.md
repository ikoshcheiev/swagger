**Update an existing pet**
----
Returns a single pet.

* **URL**

  /pet

* **Method:**

  `PUT`

*  **URL Params**

   **Required:**

   None

* **Data Params**

      {
      "id": 0,
      "category": {
        "id": 0,
        "name": "string"
      },
      "name": "doggie",
      "photoUrls": [
        "string"
      ],
      "tags": [
        {
            "id": 0,
            "name": "string"
        }
      ],
      "status": "available"
      }

* **Success Response:**

    * **Code:** 200 <br />
      **Content:**
      `````{
      "id": 0,
      "category": {
        "id": 0,
        "name": "string"
      },
      "name": "doggie",
      "photoUrls": [
        "string"
      ],
      "tags": [
        {
            "id": 0,
            "name": "string"
        }
      ],
      "status": "available"
      }

* **Error Response:**

    * **Code:** 400 <br />
      **Content:** `{ error : "Invalid ID supplied" }`

  OR

    * **Code:** 404 <br />
      **Content:** `{ error : "Pet not found" }`

  OR

    * **Code:** 405 <br />
      **Content:** `{ error : "Validation exception" }`

* **Sample Call:**

  `curl -X PUT "https://petstore.swagger.io/v2/pet" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"id\": 22330,  \"category\": {    \"id\": 0,    \"name\": \"string\"  },  \"name\": \"doggie\",  \"photoUrls\": [    \"string\"  ],  \"tags\": [    {      \"id\": 0,      \"name\": \"string\"    }  ],  \"status\": \"available\"}"`

* **Notes:**

  None