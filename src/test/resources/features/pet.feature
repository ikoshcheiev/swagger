Feature: Testing different requests on the swagger application on pet endpoints

  ########################### POST-uploadImage ############################

  @PET @POSITIVE @POST_UPLOAD_IMAGE
  Scenario Outline: Check that an image for pet can be uploaded via POST to endpoint '/pet/{petId}/uploadImage' in the swagger application
  and the success code '200' received
    Given Create pet by id <id> if not existing
    When User sends a POST with petId <id> for uploading an image <file_name> and Addition Metadata <metadata>, it must receive code <httpCode>
    Examples:
      | id     | file_name       | metadata | httpCode |
      | 123122 | cucumber.io.png | someData | 200      |

  @PET @NEGATIVE @POST_UPLOAD_IMAGE
  Scenario Outline: Check that an image for not existing pet cannot be uploaded via POST to endpoint '/pet/{petId}/uploadImage'
  in the swagger application and not Success 200 OK received
    Given Remove pet id <id> if existing
    When User sends a POST with petId <id> for uploading an image <file_name> and Addition Metadata <metadata>, it must receive code <httpCode>
    Examples:
      | id     | file_name       | metadata | httpCode |
      | 123123 | cucumber.io.png | someData | 404      |

  ############################ GET-findByStatuses ############################

  @PET @POSITIVE @GET_FIND_BY_STATUSES
  Scenario: Check that pets can be found via GET in endpoint '/pet/findByStatuses' by statuses in the swagger application and the success code '200' received
    When User sends a GET requests to the pet endpoint, it must get back the list of pets in appropriate status
      | available |
      | pending   |
      | sold      |

  @PET @NEGATIVE @GET_FIND_BY_STATUSES
  Scenario: Check that pets cannot be found via GET in endpoint '/pet/findByStatuses' by not existing status name in the swagger application and the error code '400' received
    When User sends a GET with invalid status "Invalid status value", it must get back an "400" "Invalid status value" code

############################# POST-add ############################

  @PET @POSITIVE @POST_ADD
  Scenario Outline: Check that new pet can be added via POST to endpoint '/pet' in the swagger application
  and the success code '200'-'successful operation' received
    When User sends a POST for creating new Pet by providing the information <petId>, <petName>, <categoryId>, <categoryName>, <photoUrls>, <tagIds>, <tagNames>, <petStatus>
    Then Verify that the pet with <petId> is created

    Examples:
      | petId     | petName | categoryId | categoryName | photoUrls                        | tagIds | tagNames  | petStatus |
      | 759300647 | pet_1   | 59300651   | category1    | cucumber.io.png;serenity-bdd.png | 1;2    | tag1;tag2 | available |
      | 518965346 | pet_2   | 18965337   | category2    | cucumber.io.png                  | 3      | tag1      | sold      |

  @PET @NEGATIVE @POST_ADD
  Scenario: Check that pet cannot be added via POST to endpoint '/pet' in the swagger application with invalid input
  and the error code '405'-'Invalid input' received
    When User sends a POST to endpoint for creating new Pet with invalid String id "error", it must get back error code "405", "Invalid input"

####Don't know how to emulate 405 - Invalid input

############################# PUT-update ############################

  @PET @POSITIVE @PUT_UPDATE
  Scenario Outline: Check that the existing pet can be updated via PUT to endpoint '/pet' in the swagger application
  and the success code '200' received
    Given Create pet by id <petId> if not existing
    When User sends a PUT for updating existing Pet by providing the information <petId>, <petName>, <categoryId>, <categoryName>, <photoUrls>, <tagIds>, <tagNames>, <petStatus> and status code 200, "OK"
    Then Verify the pet with <petId>, <petName>, <petStatus> is present and has correct data <categoryId>, <categoryName>, <photoUrls>, <tagIds>, <tagNames>
    Examples:
      | petId     | petName     | categoryId | categoryName      | photoUrls       | tagIds | tagNames     | petStatus |
      | 759300648 | pet_updated | 59300651   | category1_updated | cucumber.io.png | 1      | tag1_updated | available |

  @PET @NEGATIVE @PUT_UPDATE
  Scenario Outline: Check that not existing pet can not be updated via PUT to endpoint '/pet' in the swagger application
  and the error code '404' - 'Pet not found' received
    Given Remove pet id <petId> if existing
    When User sends a PUT to endpoint for updating existing Pet by providing the information <petId>, <petName>, <categoryId>, <categoryName>, <photoUrls>, <tagIds>, <tagNames>, <petStatus> and status code <httpStatus>, <CodeDescription> received
    Then Verify that the pet with <petId> is absent and an error appears <httpStatus>, "Pet not found"

    Examples:
      | petId     | petName     | categoryId | categoryName      | photoUrls                        | tagIds | tagNames          | petStatus | httpStatus | CodeDescription |
      | 759300649 | pet_updated | 59300651   | category1_updated | cucumber.io.png;serenity-bdd.png | 1;2    | tag1_updated;tag2 | available | 404        | Pet not found   |

####Don't know how to emulate 400 - Invalid ID supplied
####Don't know how to emulate 405 - Validation exception


############################# (deprecated) GET-findByTags ############################

  @PET @POSITIVE @GET_FIND_BY_TAGS
  Scenario Outline: Check that pets can be found via GET to endpoint '/pet/findByTags' by existing tag in the swagger application
  and the success code '200'-'OK' received
    Given Create pet if not existing or update existing by data: <petId>, <petName>, <categoryId>, <categoryName>, <photoUrls>, <tagIds>, <tagNames>, <petStatus>
    When User sends a GET to endpoint for receiving Pet by providing tags information <tagName>, it must receive code <httpStatus>, "OK"
    Examples:
      | petId     | petName | categoryId | categoryName | photoUrls                        | tagIds | tagNames  | petStatus | tagName | httpStatus |
      | 759300650 | pet_1   | 59300651   | category1    | cucumber.io.png;serenity-bdd.png | 1;2    | tag1;tag2 | available | tag1    | 200        |


  @PET @POSITIVE @GET_FIND_BY_TAGS
  Scenario Outline: Check that the existing pet can be sound via GET to endpoint '/pet/findByTags' by valid tag in the swagger application
  and the success code '200'-'OK' received
    When User sends a GET to endpoint for receiving Pet by providing tags information <tagName>, it must receive code <httpStatus>, "OK"
    Examples:
      | tagName    | httpStatus |
      | Tag_1      | 200        |
      | Tag_1,tag1 | 200        |
      | tag1;tag2  | 200        |

####Don't know how to emulate <400 - Invalid tag value>

############################# GET-byId ############################
  @PET @POSITIVE @GET_BY_ID @SMOKE
  Scenario Outline: Check that the existing pet can be returned via GET from endpoint '/pet/{petId}' by existing ID in the swagger application
    Given Create pet if not existing or update existing by data: <petId>, <petName>, <categoryId>, <categoryName>, <photoUrls>, <tagIds>, <tagNames>, <petStatus>
    When User sends a GET to endpoint for receiving Pet by providing Id information <petId>, it must receive proper data in Pet object <petName>, <categoryId>, <categoryName>, <photoUrls>, <tagIds>, <tagNames>, <petStatus>
    Examples:
      | petId     | petName     | categoryId | categoryName      | photoUrls                        | tagIds | tagNames          | petStatus |
      | 759300651 | pet_updated | 59300651   | category1_updated | cucumber.io.png;serenity-bdd.png | 1;2    | tag1_updated;tag2 | available |


  @PET @NEGATIVE @GET_BY_ID
  Scenario: Check that pets cannot be found via GET in endpoint '/pet/{petID}' by not valid tag in the swagger
  application and the error code 'Code'-'Description' received
    Given Remove pet id 759300652 if existing
    When User sends a GET to pet endpoint with not existing id 759300652, it must receive 404 with text "Pet not found"

####Don't know how to emulate <405 - Invalid input>


############################# POST-update ############################
# NOT WORKING HERE
  @PET @POSITIVE @POST_UPDATE
  Scenario Outline: Check that the existing pet can be updated via POST to endpoint '/pet' in the swagger application
  and the success code '200' received
    Given Create pet if not existing or update existing by data: <petId>, <petName>, <categoryId>, <categoryName>, <photoUrls>, <tagIds>, <tagNames>, <petStatus>
    When User sends a POST for updating Pet by providing the information <petId>, <petNameUpdated>, <petStatusUpdated> and status code 200, "OK"
    Then Verify that the pet with <petId> is present and has correct data <petNameUpdated>, <petStatusUpdated>
    Examples:
      | petId | petName | categoryId | categoryName | photoUrls                        | tagIds | tagNames          | petStatus | petNameUpdated | petStatusUpdated |
      | 255   | pet     | 59300651   | category1    | cucumber.io.png;serenity-bdd.png | 1;2    | tag1_updated;tag2 | available | pet_updated    | updated          |

  @PET @NEGATIVE @POST_UPDATE
  Scenario Outline: Check that not existing pet can not to be updated via POST to endpoint '/pet/{petID}' with Form Data in the swagger application
  and the error '404' - 'Pet not found' received
    Given Remove pet id <petId> if existing
    When User sends a POST for updating Pet by providing the information <petId>, <petNameUpdated>, <petStatusUpdated> and status code <httpStatus>, "Pet not found"
    Then Verify that the pet with <petId> is absent and an error appears <httpStatus>, "Pet not found"
    Examples:
      | petId     | petNameUpdated | petStatusUpdated | httpStatus |
      | 759300653 | pet_updated    | updated          | 404        |

####Don't know how to emulate <400 - Invalid ID supplied>

############################# DELETE-byId ############################

  @PET @POSITIVE @DELETE
  Scenario: Check that the existing pet can be deleted via DELETE to endpoint '/pet/{petID}' in the swagger application
  and the success code '200' received
    Given Create pet by id 759300654 if not existing
    When User sends DELETE for removing pet by 759300654, it must receive code 200, "OK"

  @PET @NEGATIVE @DELETE
  Scenario: Check that the pet cannot be deleted via DELETE to endpoint '/pet/{petID}' in the swagger application
  and the error 'Code'-'Description' received
    Given Remove pet id 759300655 if existing
    When User sends DELETE for removing pet by 759300655, it must receive code 404, "User not found"

####Don't know how to emulate <400 - Invalid ID supplied>

############################# GENERAL-error-codes ############################

  @PET @NEGATIVE @PUT_UPDATE
  Scenario Outline: Check that the PUT request to bad endpoint '/pets' in the swagger application causes the General error code '404' - 'Not Found'
    When User sends a PUT to invalid endpoint for updating existing Pet by providing the information <petId>, <petName>, <categoryId>, <categoryName>, <photoUrls>, <tagIds>, <tagNames>, <petStatus> and status code <httpStatus>, <CodeDescription>

    Examples:
      | petId     | petName     | categoryId | categoryName      | photoUrls                        | tagIds | tagNames            | petStatus | httpStatus | CodeDescription |
      | 759300656 | pet_updated | 59300651   | category1_updated | cucumber.io.png;serenity-bdd.png | 1;2    | name_updated;name_x | available | 404        | Not Found       |