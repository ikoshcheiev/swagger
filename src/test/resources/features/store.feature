Feature: Testing different requests on the swagger application on Store endpoints
  As a system user
  I want to have an ability to create, receive, delete store order in the system
  So that I can operate with any store data


  ########################### POST-placeAnOrderForPet ############################
  @STORE @POSITIVE @POST
  Scenario Outline: In order to place new order I can make POST request, then enter all data,
  then send request and receive appropriate store data in response
    When User sends a POST request for placing order with all data: <id>, <petId>, <quantity>, <shipDate>, <status>, <complete>, it must get back <httpCode> and "OK"
    Examples:
      | id   | petId | quantity | shipDate                     | status | complete | httpCode |
      | 5189 | 4     | 2        | 2020-12-21T18:56:42.560+0000 | placed | true     | 200      |

  @STORE @POSITIVE @POST
  Scenario Outline: In order to fail placing order I can make POST request, then enter all data of already created order,
  then send request and receive appropriate http code
    Given Create order if not exists with all data: <id>, <petId>, <quantity>, <shipDate>, <status>, <complete>, it must get back <httpCode>
    When User sends a POST request for placing order with all data: <id>, <petId>, <quantity>, <shipDate>, <status>, <complete>, it must get back <httpCode> and "Invalid Order"
    Examples:
      | id | petId | quantity | shipDate                     | status | complete | httpCode |
      | 5  | 123   | 2        | 2020-12-21T18:56:42.560+0000 | placed | true     | 400      |

  @STORE @NEGATIVE @POST
  Scenario Outline: In order to fail placing order I can make POST request, then enter invalid data,
  then send request and receive appropriate http code
    When User sends a POST request for placing order with all data: <id>, <petId>, <quantity>, <shipDate>, <status>, <complete>, it must get back <httpCode> and "Invalid Order"
    Examples:
      | id | petId | quantity | shipDate                     | status | complete | httpCode |
      | 0  | 0     | 0        | 2020-12-21T18:56:42.560+0000 | placed | true     | 400      |


  ########################### GET-findPurchaseOrderById ############################

  @STORE @POSITIVE @GET @SMOKE
  Scenario Outline: In order to find existing order I can make GET request, then enter all data,
  then send request and receive appropriate store data
    Given Create order if not exists with all data: <id>, <petId>, <quantity>, <shipDate>, <status>, <complete>, it must get back <SuccessCode>
    When User sends a GET for finding purchased order by Id:<id>, it must get back <SuccessCode> and contain all proper data: <petId>, <quantity>, <shipDate>, <status>, <complete>
    Then User sends a Delete for removing order by order ID: <id>, it must get back <SuccessCode> and if order wasn't existing then <NotFoundCode> "Order Not Found" or <InvalidCode> "Invalid ID supplied"
    Examples:
      | id | petId | quantity | shipDate                     | status | complete | SuccessCode | NotFoundCode | InvalidCode |
      | 5  | 123   | 2        | 2020-12-21T18:56:42.560+0000 | placed | true     | 200         | 404          | 400         |

  @STORE @NEGATIVE @GET
  Scenario Outline: In order to fail finding order I can make GET request, then enter all data of removed order,
  then send request and receive appropriate http code
    Given Remove order if exists by ID <id>
    When User sends a GET for fail finding removed purchased order by Id:<id>, it must get back http code <httpCode>, message "Order not found", type <bodyType>
    Examples:
      | id | httpCode | bodyType |
      | 5  | 404      | error    |

  @STORE @NEGATIVE @GET
  Scenario Outline: In order to fail finding order I can make GET request, then enter orderId>=11,
  then send request and receive appropriate http code
    Given Remove order if exists by ID <id>
    When User sends a GET for fail finding removed purchased order by Id:<id>, it must get back http code <httpCode>, message "Invalid ID supplied", type <bodyType>
    Examples:
      | id | httpCode | bodyType |
      | 11 | 400      | error    |


  ########################### DELETE-purchaseOrderById ############################

  @STORE @POSITIVE @DELETE
  Scenario Outline: In order to delete order I can make DELETE request, then enter integer positive existing ID,
  then send request and receive appropriate http code
    Given Create order if not exists with all data: <id>, <petId>, <quantity>, <shipDate>, <status>, <complete>, it must get back <SuccessCode>
    When User sends a Delete for removing order by order ID: <id>, it must get back <SuccessCode> and if order wasn't existing then <NotFoundCode> "Order Not Found" or <InvalidCode> "Invalid ID supplied"
    Examples:
      | id | petId | quantity | shipDate                     | status | complete | SuccessCode | NotFoundCode | InvalidCode |
      | 5  | 123   | 2        | 2020-12-21T18:56:42.560+0000 | placed | true     | 200         | 404          | 400         |

  @STORE @NEGATIVE @DELETE
  Scenario Outline: In order to fail deletion of order I can make DELETE request, then enter integer negative or not existing ID,
  then send request and receive appropriate http code
    Given Remove order if exists by ID <id>
    When User sends a Delete for removing order by order ID: <id>, it must get back <SuccessCode> and if order wasn't existing then <NotFoundCode> "Order Not Found" or <InvalidCode> "Invalid ID supplied"
    Examples:
      | id         | SuccessCode | NotFoundCode | InvalidCode |
      | 1223344556 | 200         | 404          | 400         |
      | -5         | 200         | 404          | 400         |


  ########################### GET-findPurchaseOrderById ############################

  @STORE @POSITIVE @GET
  Scenario Outline: In order to get pet inventories I can make GET request, then send request and receive list of pet statuses
    Given User sends a POST for creating new Pet by providing the information <petId>, <petName>, <categoryId>, <categoryName>, <photoUrls>, <tagIds>, <tagNames>, <petStatus>
    When User sends a GET requests for receiving pet inventories, it must get back <SuccessCode> and pet status is present <petStatus>
    Examples:
      | petId     | petName | categoryId | categoryName | photoUrls                        | tagIds | tagNames  | petStatus | petStatus | SuccessCode |
      | 518965337 | pet_1   | 59300651   | category1    | cucumber.io.png;serenity-bdd.png | 1;2    | tag1;tag2 | new       | new       | 200         |

  @STORE @POSITIVE @GET
  Scenario Outline: In order to get pet inventories and check updates I can make GET request, then send request and receive list of pet statuses
    Given Remove pet id <petId> if existing
    And User sends a GET requests for receiving pet inventories, it must get back <SuccessCode> and pet status is present <petStatus>
    And User sends a POST for creating new Pet by providing the information <petId>, <petName>, <categoryId>, <categoryName>, <photoUrls>, <tagIds>, <tagNames>, <petStatus>
    When User sends a GET requests for receiving pet inventories, it must get back <SuccessCode> and pet status is present <petStatus>
    Examples:
      | petId     | petName | categoryId | categoryName | photoUrls       | tagIds | tagNames | petStatus | SuccessCode |
      | 518965337 | pet_2   | 18965337   | category2    | cucumber.io.png | 3      | tag1     | new       | 200         |




