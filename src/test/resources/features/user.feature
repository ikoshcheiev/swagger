Feature: Testing different requests on the swagger application on User endpoints
  As a system user
  I want to have an ability to create, receive, update, delete user data in the system
  So that I can operate with any user data


  ########################### POST-createWithArray ############################
  @USER @POSITIVE @POST
  Scenario Outline: In order to create new user(s) the user should be logged in, then I can make POST request, then enter all data,
  then send request and receive appropriate user data
    Given User with data <userName> is removed from the swagger application
    When User sends a POST request for creation/updating new user with list of User's data: <id>, <userName>, <firstName>, <lastName>, <email>, <password>, <phone>, <userStatus>; it must get back <httpCode>
    Then Users with appropriate data created/updated <id>, <userName>, <firstName>, <lastName>, <email>, <password>, <phone>, <userStatus>
    And User sends a GET request for logging out to the user endpoint, it must get back 200
    Examples:
      | id        | userName        | firstName        | lastName        | email                         | password    | phone                     | userStatus | httpCode |
      | 5189      | username1       | firstname1       | lastName1       | email1@abc.org                | pass1       | 971-279-2288              | 1          | 200      |
      | 5189;5190 | username1;name2 | firstname1;name2 | lastName1;name2 | email1@abc.org;email2@abc.org | pass1;pass2 | 971-279-2288;971-111-2222 | 1;0        | 200      |

  @USER @POSITIVE @POST
  Scenario Outline: In order to update existing user(s) I can make POST request, then enter all data,
  then send request and receive appropriate user data
    Given User with data <id>, <userName>, <firstName>, <lastName>, <email>, <password>, <phone>, <userStatus> is created in the swagger application
    When User sends a POST request for creation/updating new user with list of User's data: <id>, <key>, <key>, <key>, <key>, <key>, <key>, <keyLength>; it must get back <httpCode>>
    Then Users with appropriate data created/updated <id>, <key>, <key>, <key>, <key>, <key>, <key>, <keyLength>
    Examples:
      | id   | userName  | firstName  | lastName  | email          | password | phone        | userStatus | key      | keyLength | httpCode |
      | 5189 | username1 | firstname1 | lastName1 | email1@abc.org | pass1    | 971-279-2288 | 1          | _updated | 8         | 200      |


#### 400 - Bad Input
#### default - successful operation

  ########################### POST-createWithList ############################
  @USER @POSITIVE @POST
  Scenario Outline: In order to create new user(s) I can make POST request, then enter all data,
  then send request and receive appropriate user data
    Given User with data <userName> is removed from the swagger application
    When User sends a POST request for creation new user with array of User's data: <id>, <userName>, <firstName>, <lastName>, <email>, <password>, <phone>, <userStatus>; it must get back <httpCode>
    Then Users with appropriate data created/updated <id>, <userName>, <firstName>, <lastName>, <email>, <password>, <phone>, <userStatus>
    Examples:
      | id        | userName        | firstName        | lastName        | email                         | password    | phone                     | userStatus | httpCode |
      | 5189      | username1       | firstname1       | lastName1       | email1@abc.org                | pass1       | 971-279-2288              | 1          | 200      |
      | 5189;5190 | username1;name2 | firstname1;name2 | lastName1;name2 | email1@abc.org;email2@abc.org | pass1;pass2 | 971-279-2288;971-111-2222 | 1;0        | 200      |


#### 400 - Bad Input
#### default - successful operation


  ########################### POST-createUser ############################

  @USER @POSITIVE @POST
  Scenario Outline: In order to create new user I can make POST request, then enter all data,
  then send request and receive appropriate user data
    Given User with data <userName> is removed from the swagger application
    When User sends a POST request for creation new user with data: <id>, <userName>, <firstName>, <lastName>, <email>, <password>, <phone>, <userStatus>, it must get back <httpCode>
    Then Users with appropriate data created/updated <id>, <userName>, <firstName>, <lastName>, <email>, <password>, <phone>, <userStatus>
    Examples:
      | id   | userName  | firstName  | lastName  | email          | password | phone        | userStatus | httpCode |
      | 5189 | username1 | firstname1 | lastName1 | email1@abc.org | pass1    | 971-279-2288 | 1          | 200      |


  @USER @NEGATIVE @POST
  Scenario Outline: In order to create new user I can make POST request, then enter all data,
  then send request and receive appropriate user data
    Given User with data <id>, <userName>, <firstName>, <lastName>, <email>, <password>, <phone>, <userStatus> is created in the swagger application
    When User sends a POST request for creation new user with data: <id>, <key>, <key>, <lastName>, <email>, <password>, <phone>, <keyLength>, it must get back <httpCode>
    Then Users with appropriate data created/updated <id>, <key>, <key>, <lastName>, <email>, <password>, <phone>, <keyLength>
    Examples:
      | id   | userName  | firstName  | lastName  | email          | password | phone        | userStatus | key      | keyLength | httpCode |
      | 5189 | username1 | firstname1 | lastName1 | email1@abc.org | pass1    | 971-279-2288 | 1          | _updated | 8         | 200      |

#  default - successful operation

  ########################### PUT-UpdateUser ############################

  @USER @POSITIVE @PUT
  Scenario Outline: In order to update user I can make PUT request, then enter all data,
  then send request and receive appropriate user data
    Given User with data <id>, <userName>, <firstName>, <lastName>, <email>, <password>, <phone>, <userStatus> is created in the swagger application
    When User sends a PUT request for updating user with data: <id>, <userName>, <firstName>, <key>, <email>, <key>, <phone>, <keyLength>, it must get back <httpCode>
    Then Users with appropriate data created/updated <id>, <userName>, <firstName>, <key>, <email>, <key>, <phone>, <keyLength>
    Examples:
      | id   | userName  | firstName  | lastName  | email          | password | phone        | userStatus | key      | keyLength | httpCode |
      | 5189 | username1 | firstname1 | lastName1 | email1@abc.org | pass1    | 971-279-2288 | 1          | _updated | 8         | 200      |


  @USER @NEGATIVE @PUT
  Scenario Outline: In order to update user I can make PUT request, then enter all data,
  then send request and receive appropriate user data
    Given User with data <userName> is removed from the swagger application
    When User sends a PUT request for updating user with data: <id>, <key>, <firstName>, <lastName>, <email>, <password>, <key>, <keyLength>, it must get back <httpCode>
    Then Users with appropriate data created/updated <id>, <key>, <firstName>, <lastName>, <email>, <password>, <key>, <keyLength>
    Examples:
      | id   | userName  | firstName  | lastName  | email          | password | key      | keyLength | httpCode |
      | 5189 | username1 | firstname1 | lastName1 | email1@abc.org | pass1    | _updated | 8         | 200      |

#### 400 - Invalid user supplied
#### 404 - User not found

  ########################### DELETE-DeleteUser ############################

  @USER @POSITIVE @DELETE
  Scenario Outline: In order to delete user I can make DELETE request, then enter all data,
  then send request and receive appropriate response
    Given User with data <id>, <userName>, <firstName>, <lastName>, <email>, <password>, <phone>, <userStatus> is created in the swagger application
    When User sends a DELETE request for removing user with <userName> from the swagger application, it must receive code <httpCode>, "OK"
    Then User sends a GET request to not existing user endpoint by <userName>, it must get back error 404 and "User not found" message
    Examples:
      | id   | userName  | firstName  | lastName  | email          | password | phone        | userStatus | httpCode |
      | 5189 | username1 | firstname1 | lastName1 | email1@abc.org | pass1    | 971-279-2288 | 1          | 200      |


  @USER @NEGATIVE @DELETE
  Scenario Outline: In order to delete user I can make DELETE request, then enter all data,
  then send request and receive appropriate response
    Given User with data <userName> is removed from the swagger application
    When User sends a DELETE request for removing user with <userName> from the swagger application, it must receive code <httpCode>, "User not found"
    Examples:
      | userName  | httpCode |
      | username1 | 404      |

  ########################### GET-userByName ############################
  @USER @POSITIVE @GET @SMOKE
  Scenario Outline: In order to find user I can make GET request, then enter exact name,
  then send request and receive appropriate user data
    Given User with data <id>, <userName>, <firstName>, <lastName>, <email>, <password>, <phone>, <userStatus> is created in the swagger application
    When User sends a GET request to the user endpoint by <userName>, it must get back userdata <id>, <firstName>, <lastName>, <email>, <password>, <phone>, <userStatus>, it must get back <httpCode>
    Examples:
      | id   | userName  | firstName  | lastName  | email          | password | phone        | userStatus | httpCode |
      | 5189 | username1 | firstname1 | lastName1 | email1@abc.org | pass1    | 971-279-2288 | 1          | 200      |

  @USER @NEGATIVE @GET
  Scenario Outline: In order to find not existing user I can make GET request, then enter exact name,
  then send request and receive appropriate error message
    Given User with data <userName> is removed from the swagger application
    When User sends a GET request to not existing user endpoint by <userName>, it must get back error <httpCode> and "User not found" message
    Examples:
      | userName  | httpCode |
      | userName1 | 404      |

#### Don't know how to reproduce 400 Invalid username supplied

    ########################### GET-LogUserIn ############################
  @USER @POSITIVE @GET
  Scenario Outline: In order to log in user I can make GET request, then enter exact user name and password,
  then send request and receive appropriate user data
    Given User with data <id>, <userName>, <firstName>, <lastName>, <email>, <password>, <phone>, <userStatus> is created in the swagger application
    When User sends a GET request for logging in to the user endpoint by login:<userName>, password:<password>, it must get back message "logged in user session:" with timestamp at the end
    Examples:
      | id   | userName  | firstName  | lastName  | email          | password | phone        | userStatus |
      | 5189 | username1 | firstname1 | lastName1 | email1@abc.org | pass1    | 971-279-2288 | 1          |

  @USER @NEGATIVE @GET
  Scenario Outline: In order to fail log in user I can make GET request, then enter not existing user name and good password,
  then send request and receive appropriate user data
    Given User with data <userName> is removed from the swagger application
    When User sends a GET request for logging in to the user endpoint by <userName> and <password>, it must get back <httpCode> and message "Invalid username/password supplied"
    Examples:
      | userName    | password | httpCode |
      | notExisting | pass1    | 400      |

  @USER @NEGATIVE @GET
  Scenario Outline: In order to fail log in user I can make GET request, then enter exact user name and invalid password,
  then send request and receive appropriate user data
    Given User with data <id>, <userName>, <firstName>, <lastName>, <email>, <password>, <phone>, <userStatus> is created in the swagger application
    When User sends a GET request for logging in to the user endpoint by <userName> and "invalidPass", it must get back <httpCode> and message "Invalid username/password supplied"
    Examples:
      | id   | userName  | firstName  | lastName  | email          | password | phone        | userStatus | httpCode |
      | 5189 | username1 | firstname1 | lastName1 | email1@abc.org | pass1    | 971-279-2288 | 1          | 400      |

    ########################### GET-LogUserOut ############################
  @USER @POSITIVE @GET
  Scenario Outline: In order to log out current logged in users I can make GET request, then send request and receive status code 200 successful operation
    Given User with data <id>, <userName>, <firstName>, <lastName>, <email>, <password>, <phone>, <userStatus> is created in the swagger application
    And User sends a GET request for logging in to the user endpoint by <userName> and <password>, it must get back <httpCode> and message "HTTP/1.1 200 OK"
    When User sends a GET request for logging out to the user endpoint, it must get back <httpCode>
    Examples:
      | id   | userName  | firstName  | lastName  | email          | password | phone        | userStatus | httpCode |
      | 5189 | username1 | firstname1 | lastName1 | email1@abc.org | pass1    | 971-279-2288 | 1          | 200      |

  @USER @NEGATIVE @GET
  Scenario Outline: In order to be sure that I can logging out without errors when all users are already logged out I can make GET request, then make sure that previously nobody logged in,
  then send request and receive status code 200 successful operation
    Given User with data <id>, <userName>, <firstName>, <lastName>, <email>, <password>, <phone>, <userStatus> is created in the swagger application
    And User sends a GET request for logging in to the user endpoint by <userName> and <password>
    And User sends a GET request for logging out to the user endpoint, it must get back <httpCode>
    When User sends a GET request for logging out to the user endpoint, it must get back <httpCode>
    Examples:
      | id   | userName  | firstName  | lastName  | email          | password | phone        | userStatus | httpCode |
      | 5189 | username1 | firstname1 | lastName1 | email1@abc.org | pass1    | 971-279-2288 | 1          | 200      |


